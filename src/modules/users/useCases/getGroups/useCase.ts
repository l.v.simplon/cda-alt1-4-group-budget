import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class JoinGroupUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const user = await prisma.user.findUnique({
      where: { id: request.userId },
      include: { groups: true }
    })

    return user?.groups;
  }
}
