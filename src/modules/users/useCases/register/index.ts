import RegisterController from '../../../../framework/Controller'
import { RegisterUseCase } from './useCase'

export { RegisterController, RegisterUseCase };
