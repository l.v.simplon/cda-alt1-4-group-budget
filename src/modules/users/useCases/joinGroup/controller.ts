import * as express from 'express'

import { BaseController } from "../../../../framework/ControllerI"

import { JoinGroupUseCase } from "./useCase";
import { DTO } from "./dto";

export class JoinGroupController extends BaseController {
  constructor(
    private useCase: JoinGroupUseCase
  ) {
    super();
  }

  async  executeImpl ( req: express.Request, res: express.Response ) : Promise<any> {

    const dto = req.body as DTO;
    this.useCase.execute(dto);
  
    return this.ok<any>(res);
  }
}
  