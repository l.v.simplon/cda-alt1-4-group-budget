import { AddRefundUseCase, AddRefundController } from '../useCases/addRefund'

import * as express from 'express'
import { Router } from 'express'


export function buildRouter(userRepository, groupRepository) {
    const addRefundUseCase = new AddRefundUseCase(userRepository, groupRepository);
    const addRefundController = new AddRefundController(addRefundUseCase);
        
    const userRouter: Router = Router();

    userRouter.post('/addRefund', (req, res) => addRefundController.execute(req, res) );

    return { userRouter }
};
