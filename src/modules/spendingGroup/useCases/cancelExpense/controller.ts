import * as express from 'express'

import { BaseController } from "../../../../framework/ControllerI"

import { CancelExpenseUseCase } from "./useCase";
import { DTO } from "./dto";

export default class CancelExpenseController extends BaseController {
  constructor(
    private useCase: CancelExpenseUseCase
  ) {
    super();
  }

  async  executeImpl ( req: express.Request, res: express.Response ) : Promise<any> {

    const dto = req.body as DTO;
    this.useCase.execute(dto);
  
    return this.ok<any>(res);
  }
}
  