import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class AddExpenseUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const update = await prisma.expense.create({
      data: {
        amount: request.amount,
        text: request.text,
        fromUser: { connect: { id: request.userId, } },
        group:    { connect: { id: request.groupId, },
        },
      },
    });

  }
}
