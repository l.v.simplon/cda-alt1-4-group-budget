import { AddExpenseUseCase, AddExpenseController } from '../useCases/addExpense';
import { CancelExpenseUseCase, CancelExpenseController } from '../useCases/cancelExpense';
import { CreateGroupUseCase, CreateGroupController } from '../useCases/createGroup';
import { CreateGroupInviteLinkUseCase, CreateGroupInviteLinkController } from '../useCases/createGroupInviteLink';
import { GetGroupUseCase, GetGroupController } from '../useCases/getGroup';

import * as express from 'express'
import { Router } from 'express'


export default function buildRouter(userRepository, groupRepository) {
    const expenseRouter: Router = Router();

    const addExpenseUseCase = new AddExpenseUseCase(userRepository, groupRepository);
    const addExpenseController = new AddExpenseController(addExpenseUseCase);
    expenseRouter.post('/addExpense', (req, res) => addExpenseController.execute(req, res) );


    const cancelExpenseUseCase = new CancelExpenseUseCase(userRepository, groupRepository);
    const cancelExpenseController = new CancelExpenseController(cancelExpenseUseCase);
    expenseRouter.post('/cancelExpense', (req, res) => cancelExpenseController.execute(req, res) );


    const createGroupUseCase = new CreateGroupUseCase(userRepository, groupRepository);
    const createGroupController = new CreateGroupController(createGroupUseCase);
    expenseRouter.post('/createGroup', (req, res) => createGroupController.execute(req, res) );


    const createGroupInviteLinkUseCase = new CreateGroupInviteLinkUseCase(groupRepository);
    const createGroupInviteLinkController = new CreateGroupInviteLinkController(createGroupInviteLinkUseCase);
    expenseRouter.post('/createGroupLink', (req, res) => createGroupInviteLinkController.execute(req, res) );



    const getGroupUseCase = new GetGroupUseCase(groupRepository);
    const getGroupController = new GetGroupController(getGroupUseCase);
    expenseRouter.post('/getGroup', (req, res) => getGroupController.execute(req, res) );


    return expenseRouter;
};
