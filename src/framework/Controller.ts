import * as express from 'express'

import { BaseController } from "./ControllerI"

import { UseCase } from "./UseCaseI";

export default class CreateGroupController extends BaseController {
  constructor(
    private useCase: UseCase<any, any>
  ) {
    super();
  }

  async  executeImpl ( req: express.Request, res: express.Response ) : Promise<any> {

    const dto = req.body;
    this.useCase.execute(dto);
  
    return this.ok<any>(res);
  }
}
  